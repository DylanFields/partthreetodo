import React, { Component } from "react";
import "./index.css";
import todosList from "./todos.json";
import { Route, Link } from 'react-router-dom';
import { connect } from "react-redux";
import { addTodo, clearCompletedTodos } from "./actions";
import TodoList from "./TodoList"

class App extends Component {
  state = {
    todos: todosList
  };
  handleClearCompletedTodos = event => {
    console.log("todos were cleared");
    this.props.clearCompletedTodos();
  };



  handleCreateTodo = event => {
    if (event.key === "Enter") {
      this.props.addTodo(event.target.value);
      event.target.value = "";
    }
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autofocus
            onKeyDown={this.handleCreateTodo}

          />
        </header>
        <Route exact path="/">
          <TodoList
            todos={this.props.todos}
          />
        </Route>
        <Route exact path="/active">
          <TodoList
            todos={this.props.todos.filter(todo => todo.completed === false)}
          />
        </Route>
        <Route exact path="/completed">
          <TodoList
            todos={this.props.todos.filter(todo => todo.completed === true)}
          />
        </Route>

        <footer className="footer">
          {/* <!-- This should be `0 items left` by default --> */}
          <span className="todo-count">
            <strong>0</strong> item(s) left

          </span>
          <ul className="filters">
            <li>
              <Link to="/">All</Link>
            </li>
            <li>
              <Link to="/active">Active</Link>
            </li>
            <li>
              <Link to="/completed">Completed</Link >
            </li>
          </ul>
          <button
            className="clear-completed"
            onClick={this.handleClearCompletedTodos}
          >
            Clear completed
    </button>
        </footer>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    todos: state.todos
  };
};

const mapDispatchToProps = {
  addTodo,
  clearCompletedTodos,
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
